﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TodoList.Data.Repositories;
using TodoList.Data.Infrastructure;
using TodoList.Data;
using TodoList.Model.Models;
using TodoList.Tests.Framework;
using System.Linq;
using NFluent;

namespace TodoList.Tests.DataLayerTests
{
    [TestClass]
    public class TodoEntryRepositoryTest
    {
        private IDbFactory dbFactory;
        private IUnitOfWork uow;
        private IUserRepository userRepository;
        private ITodoEntryRepository todoEntryRepository;

        [TestInitialize]
        public void TestInitialize()
        {
            EffortProviderFactory.ResetDb();
            dbFactory = new DbFactory();
            uow = new UnitOfWork(dbFactory);
            userRepository = new UserRepository(dbFactory);
            todoEntryRepository = new TodoEntryRepository(dbFactory);
        }

        [TestMethod]
        public void Add_Should_Create_A_New_TodoEntry()
        {
            // Arrange
            DataHelpers.InitializeData();
            var newTodoEntry = new TodoEntry()
            {
                DueDate = new DateTime(2017,08,01),
                Priority = TodoEntryPriorities.High,
                Text = "New Task",
                UserID = 1,
                Completed = false
            };

            // Act
            todoEntryRepository.Add(newTodoEntry);
            uow.Commit();

            // Assert
            Assert.AreNotEqual(0, newTodoEntry.TodoEntryID);
            var addedTodoEntry = todoEntryRepository.GetById(newTodoEntry.TodoEntryID);
            Assert.AreEqual(addedTodoEntry.DueDate, new DateTime(2017, 08, 01));
            Assert.AreEqual(addedTodoEntry.Priority, TodoEntryPriorities.High);
            Assert.AreEqual(addedTodoEntry.Text, "New Task");
        }

        [TestMethod]
        public void Update_Should_Modify_An_Existing_TodoEntry()
        {
            // Arrange
            DataHelpers.InitializeData();
            var existingTodoEntry = todoEntryRepository.GetById(1);

            var savedTodoEntry = new TodoEntry()
            {
                DueDate = existingTodoEntry.DueDate,
                Priority = existingTodoEntry.Priority,
                Text = existingTodoEntry.Text,
                TodoEntryID = existingTodoEntry.TodoEntryID,
                Completed = existingTodoEntry.Completed
            };

            existingTodoEntry.DueDate = new DateTime(2017, 08, 01);
            existingTodoEntry.Priority = TodoEntryPriorities.Medium;
            existingTodoEntry.Text = "Updated Task";
            existingTodoEntry.Completed = !existingTodoEntry.Completed;


            // Act
            todoEntryRepository.Update(existingTodoEntry);
            uow.Commit();

            // Assert
            existingTodoEntry = todoEntryRepository.GetById(savedTodoEntry.TodoEntryID);

            Assert.AreEqual(savedTodoEntry.TodoEntryID, existingTodoEntry.TodoEntryID);
            Assert.AreNotEqual(savedTodoEntry.DueDate, existingTodoEntry.DueDate);
            Assert.AreNotEqual(savedTodoEntry.Priority, existingTodoEntry.Priority);
            Assert.AreNotEqual(savedTodoEntry.Text, existingTodoEntry.Text);
            Assert.AreNotEqual(savedTodoEntry.Completed, existingTodoEntry.Completed);
        }

        [TestMethod]
        public void Delete_Should_Remove_An_Existing_TodoEntry()
        {
            // Arrange
            DataHelpers.InitializeData();
            var existingTodoEntry = todoEntryRepository.GetById(1);

            // Act
            todoEntryRepository.Delete(existingTodoEntry);
            uow.Commit();

            // Assert
            var deletedTodoEntry = todoEntryRepository.GetById(1);

            Assert.IsNull(deletedTodoEntry);
        }

        [TestMethod]
        public void GetAll_Should_Return_All_TodoEntries()
        {
            // Arrange
            DataHelpers.InitializeData();

            // Act
            var todoEntries = todoEntryRepository.GetAll();

            // Assert
            Assert.AreEqual(4,todoEntries.Count());

        }

        [TestMethod]
        public void GetAll_Should_Return_All_TodoEntries_Sorted_Ascending_By_DueDate()
        {
            // Arrange
            DataHelpers.InitializeData();

            // Act
            var todoEntries = todoEntryRepository.GetAll(new SortProperty[] { new SortProperty("DueDate", SortPropertyDirection.Ascending) }).ToList();

            // Assert
            Assert.AreEqual(4, todoEntries.Count());
            Assert.AreEqual(1, todoEntries[0].TodoEntryID);
            Assert.AreEqual(3, todoEntries[1].TodoEntryID);
            Assert.AreEqual(4, todoEntries[2].TodoEntryID);
            Assert.AreEqual(2, todoEntries[3].TodoEntryID);
        }

        [TestMethod]
        public void GetAll_Should_Return_All_TodoEntries_Sorted_Descending_By_DueDate()
        {
            // Arrange
            DataHelpers.InitializeData();

            // Act
            var todoEntries = todoEntryRepository.GetAll(new SortProperty[] { new SortProperty("DueDate", SortPropertyDirection.Descending) }).ToList();

            // Assert
            Assert.AreEqual(4, todoEntries.Count());
            Assert.AreEqual(2, todoEntries[0].TodoEntryID);
            Assert.AreEqual(4, todoEntries[1].TodoEntryID);
            Assert.AreEqual(3, todoEntries[2].TodoEntryID);
            Assert.AreEqual(1, todoEntries[3].TodoEntryID);
        }

        [TestMethod]
        public void GetAll_Should_Return_All_TodoEntries_Sorted_Ascending_By_Priority()
        {
            // Arrange
            DataHelpers.InitializeData();

            // Act
            var todoEntries = todoEntryRepository.GetAll(new SortProperty[] { new SortProperty("Priority", SortPropertyDirection.Ascending) }).ToList();

            // Assert
            Assert.AreEqual(4, todoEntries.Count());
            Assert.AreEqual(1, todoEntries[0].TodoEntryID);
            Assert.AreEqual(4, todoEntries[1].TodoEntryID);
            Assert.AreEqual(2, todoEntries[2].TodoEntryID);
            Assert.AreEqual(3, todoEntries[3].TodoEntryID);
        }

        [TestMethod]
        public void GetAll_Should_Return_All_TodoEntries_Sorted_Descending_By_Priority()
        {
            // Arrange
            DataHelpers.InitializeData();

            // Act
            var todoEntries = todoEntryRepository.GetAll(new SortProperty[] { new SortProperty("Priority", SortPropertyDirection.Descending) }).ToList();

            // Assert
            Assert.AreEqual(4, todoEntries.Count());
            Assert.AreEqual(3, todoEntries[0].TodoEntryID);
            Assert.AreEqual(2, todoEntries[1].TodoEntryID);
            Assert.AreEqual(1, todoEntries[2].TodoEntryID);
            Assert.AreEqual(4, todoEntries[3].TodoEntryID);
        }

        [TestMethod]
        public void GetAll_Should_Return_All_TodoEntries_Sorted_Ascending_By_DueDate_Then_Ascending_By_Priority()
        {
            // Arrange
            DataHelpers.InitializeData();

            // Act
            var todoEntries = todoEntryRepository.GetAll(new SortProperty[] { new SortProperty("DueDate", SortPropertyDirection.Ascending), new SortProperty("Priority", SortPropertyDirection.Ascending) }).ToList();

            // Assert
            Assert.AreEqual(4, todoEntries.Count());
            Assert.AreEqual(1, todoEntries[0].TodoEntryID);
            Assert.AreEqual(3, todoEntries[1].TodoEntryID);
            Assert.AreEqual(4, todoEntries[2].TodoEntryID);
            Assert.AreEqual(2, todoEntries[3].TodoEntryID);
        }

        [TestMethod]
        public void GetAll_Should_Return_All_TodoEntries_Sorted_Descending_By_DueDate_Then_Descending_By_Priority()
        {
            // Arrange
            DataHelpers.InitializeData();

            // Act
            var todoEntries = todoEntryRepository.GetAll(new SortProperty[] { new SortProperty("DueDate", SortPropertyDirection.Descending), new SortProperty("Priority", SortPropertyDirection.Descending) }).ToList();

            // Assert
            Assert.AreEqual(4, todoEntries.Count());
            Assert.AreEqual(2, todoEntries[0].TodoEntryID);
            Assert.AreEqual(4, todoEntries[1].TodoEntryID);
            Assert.AreEqual(3, todoEntries[2].TodoEntryID);
            Assert.AreEqual(1, todoEntries[3].TodoEntryID);
        }

        [TestMethod]
        public void GetAll_Should_Return_All_TodoEntries_Sorted_Ascending_By_Priority_Then_Ascending_By_DueDate()
        {
            // Arrange
            DataHelpers.InitializeData();

            // Act
            var todoEntries = todoEntryRepository.GetAll(new SortProperty[] { new SortProperty("Priority", SortPropertyDirection.Ascending), new SortProperty("DueDate", SortPropertyDirection.Ascending) }).ToList();

            // Assert
            Assert.AreEqual(4, todoEntries.Count());
            Assert.AreEqual(1, todoEntries[0].TodoEntryID);
            Assert.AreEqual(4, todoEntries[1].TodoEntryID);
            Assert.AreEqual(2, todoEntries[2].TodoEntryID);
            Assert.AreEqual(3, todoEntries[3].TodoEntryID);
        }

        [TestMethod]
        public void GetAll_Should_Return_All_TodoEntries_Sorted_Descending_By_Priority_Then_Descending_By_DueDate()
        {
            // Arrange
            DataHelpers.InitializeData();

            // Act
            var todoEntries = todoEntryRepository.GetAll(new SortProperty[] { new SortProperty("Priority", SortPropertyDirection.Descending), new SortProperty("DueDate", SortPropertyDirection.Descending) }).ToList();

            // Assert
            Assert.AreEqual(4, todoEntries.Count());
            Assert.AreEqual(3, todoEntries[0].TodoEntryID);
            Assert.AreEqual(2, todoEntries[1].TodoEntryID);
            Assert.AreEqual(4, todoEntries[2].TodoEntryID);
            Assert.AreEqual(1, todoEntries[3].TodoEntryID);
        }
    }
}
