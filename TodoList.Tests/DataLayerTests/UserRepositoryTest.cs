﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TodoList.Data.Repositories;
using TodoList.Data.Infrastructure;
using TodoList.Data;
using TodoList.Model.Models;
using TodoList.Tests.Framework;
using System.Linq;

namespace TodoList.Tests.DataLayerTests
{
    [TestClass]
    public class UserRepositoryTest
    {
        private IDbFactory dbFactory;
        private IUnitOfWork uow;
        private IUserRepository userRepository;

        [TestInitialize]
        public void TestInitialize()
        {
            EffortProviderFactory.ResetDb();
            dbFactory = new DbFactory();
            uow = new UnitOfWork(dbFactory);
            userRepository = new UserRepository(dbFactory);
        }

        [TestMethod]
        public void Add_Should_Create_A_New_User()
        {
            // Arrange
            DataHelpers.InitializeData();


            var newUser = new User()
            {
                Email = "newuser@somedomain.com",
                Name = "New User",
                Password = "password"
            };

            newUser.TodoList.Add(new TodoEntry()
            {
                DueDate = new DateTime(2017, 08, 01),
                Priority = TodoEntryPriorities.High,
                Text = "New Task"
            });

            // Act
            userRepository.Add(newUser);
            uow.Commit();

            // Assert
            Assert.AreNotEqual(0, newUser.UserID);

            var user = userRepository.GetById(newUser.UserID);

            Assert.AreEqual(newUser.Email, "newuser@somedomain.com");
            Assert.AreEqual(newUser.Name, "New User");
            Assert.AreEqual(newUser.Password, "password");
            Assert.AreEqual(newUser.TodoList[0].DueDate, new DateTime(2017, 08, 01));
            Assert.AreEqual(newUser.TodoList[0].Priority, TodoEntryPriorities.High);
            Assert.AreEqual(newUser.TodoList[0].Text, "New Task");
        }

        [TestMethod]
        public void Update_Should_Modify_An_Existing_User()
        {
            // Arrange
            DataHelpers.InitializeData();
            var existingUser = userRepository.GetById(1);
            var savedUser = new User()
            {
                Email = existingUser.Email,
                Name = existingUser.Name,
                Password = existingUser.Password,
                UserID = existingUser.UserID
            };

            foreach(var existingTodoEntry in existingUser.TodoList)
            {
                savedUser.TodoList.Add(new TodoEntry()
                {
                    DueDate = existingTodoEntry.DueDate,
                    Priority = existingTodoEntry.Priority,
                    Text = existingTodoEntry.Text
                });
            }

            existingUser.Email = "newemail@somedomain.com";
            existingUser.Name = "New Name";
            existingUser.Password = "newpassword";
            existingUser.TodoList[0].DueDate = DateTime.Today.AddDays(-7);
            existingUser.TodoList[0].Priority = TodoEntryPriorities.Medium;
            existingUser.TodoList[0].Text = "Updated Task";

            // Act
            userRepository.Update(existingUser);
            uow.Commit();

            // Assert
            existingUser = userRepository.GetById(savedUser.UserID);

            Assert.AreEqual(savedUser.UserID, existingUser.UserID);
            Assert.AreNotEqual(savedUser.Email, existingUser.Email);
            Assert.AreNotEqual(savedUser.Name, existingUser.Name);
            Assert.AreNotEqual(savedUser.Password, existingUser.Password);
            Assert.AreNotEqual(savedUser.TodoList[0].DueDate, existingUser.TodoList[0].DueDate);
            Assert.AreNotEqual(savedUser.TodoList[0].Priority, existingUser.TodoList[0].Priority);
            Assert.AreNotEqual(savedUser.TodoList[0].Text, existingUser.TodoList[0].Text);
        }

        [TestMethod]
        public void Delete_Should_Remove_An_Existing_User()
        {
            // Arrange
            DataHelpers.InitializeData();
            var existingUser = userRepository.GetById(1);

            // Act
            userRepository.Delete(existingUser);
            uow.Commit();

            // Assert
            var deletedUser = userRepository.GetById(1);

            Assert.IsNull(deletedUser);
        }

        [TestMethod]
        public void GetAll_Should_Return_All_Users()
        {
            // Arrange
            DataHelpers.InitializeData();

            // Act
            var users = userRepository.GetAll();

            // Assert
            Assert.AreEqual(2, users.Count());

        }

        [TestMethod]
        public void Get_User_By_Email_Should_Return_An_Existing_User()
        {
            // Arrange
            DataHelpers.InitializeData();

            // Act
            var existingUser = userRepository.Get(p => p.Email == "email@somedomain.com");

            // Assert
            Assert.IsNotNull(existingUser);
            Assert.AreEqual(existingUser.UserID, 1);
        }

        [TestMethod]
        public void Get_User_By_Email_Should_Return_Null()
        {
            // Arrange
            DataHelpers.InitializeData();

            // Act
            var existingUser = userRepository.Get(p => p.Email == "xemail@somedomain.com");

            // Assert
            Assert.IsNull(existingUser);
        }
    }
}
