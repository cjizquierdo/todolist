﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TodoList.Service;
using Moq;
using TodoList.Data.Repositories;
using TodoList.Data.Infrastructure;
using System.Linq;
using TodoList.Tests.Framework;
using TodoList.Model.Models;

namespace TodoList.Tests.ServiceLayerTests
{
    [TestClass]
    public class UserServiceTest
    {
        private Mock<IValidationDictionary> validationDictionary;
        private Mock<IUserRepository> userRepository;
        private Mock<ITodoEntryRepository> todoEntryRepository;
        private Mock<IUnitOfWork> unitOfWork;
        private IUserService userService;

        [TestInitialize]
        public void TestInitialize()
        {
            validationDictionary = new Mock<IValidationDictionary>();
            userRepository = new Mock<IUserRepository>();
            todoEntryRepository = new Mock<ITodoEntryRepository>();
            unitOfWork = new Mock<IUnitOfWork>();
            userService = new UserService(validationDictionary.Object, userRepository.Object, todoEntryRepository.Object, unitOfWork.Object);
        }

        [TestMethod]
        public void Get_User()
        {
            // Arrange
            var userOne = DataHelpers.UserOne;
            userRepository.Setup(r => r.GetById(1)).Returns(userOne);

            // Act
            var user = userService.Get(1);

            // Assert
            Assert.IsNotNull(user);
            Assert.AreEqual(userOne.Email, user.Email);
            Assert.AreEqual(userOne.Name, user.Name);
            Assert.AreEqual(userOne.Password, user.Password);
            Assert.AreEqual("Task 1", user.TodoList[0].Text);
            Assert.AreEqual("Task 2", user.TodoList[1].Text);
            Assert.AreEqual("Task 3", user.TodoList[2].Text);
        }

        [TestMethod]
        public void Get_User_Should_Return_Null_When_Throws_Execption()
        {
            // Arrange
            userRepository.Setup(r => r.GetById(1)).Throws(new Exception());

            // Act
            var user = userService.Get(1);

            // Assert
            Assert.IsNull(user);
        }
    }
}
