﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TodoList.Service;
using Moq;
using TodoList.Data.Repositories;
using TodoList.Data.Infrastructure;
using System.Linq;
using TodoList.Tests.Framework;
using TodoList.Model.Models;

namespace TodoList.Tests.ServiceLayerTests
{
    [TestClass]
    public class TodoEntryServiceTest
    {
        private Mock<IValidationDictionary> validationDictionary;
        private Mock<ITodoEntryRepository> todoEntryRepository;
        private Mock<IUserRepository> userRepository;
        private Mock<IUnitOfWork> unitOfWork;
        private ITodoEntryService todoEntryService;

        [TestInitialize]
        public void TestInitialize()
        {
            validationDictionary = new Mock<IValidationDictionary>();
            todoEntryRepository = new Mock<ITodoEntryRepository>();
            userRepository = new Mock<IUserRepository>();
            unitOfWork = new Mock<IUnitOfWork>();
            todoEntryService = new TodoEntryService(validationDictionary.Object, userRepository.Object, todoEntryRepository.Object, unitOfWork.Object);
        }

        [TestMethod]
        public void Get_User_TodoList()
        {
            // Arrange
            userRepository.Setup(r => r.GetById(1)).Returns(DataHelpers.UserOne);

            // Act
            var userTodoList = todoEntryService.GetUserTodoList(1).ToList();

            // Assert
            Assert.AreEqual(3, userTodoList.Count());
            Assert.AreEqual("Task 1", userTodoList[0].Text);
            Assert.AreEqual("Task 2", userTodoList[1].Text);
            Assert.AreEqual("Task 3", userTodoList[2].Text);
        }

        [TestMethod]
        public void Get_User_TodoList_Should_Return_Null_When_Throws_Execption()
        {
            // Arrange
            userRepository.Setup(r => r.GetById(1)).Throws(new Exception());

            // Act
            var userTodoList = todoEntryService.GetUserTodoList(1);

            // Assert
            Assert.IsNull(userTodoList);
        }

        [TestMethod]
        public void Add_Valid_Entry_To_User_TodoList()
        {
            // Arrange
            validationDictionary.Setup(r => r.IsValid).Returns(true);
            userRepository.Setup(r => r.GetById(1)).Returns(DataHelpers.UserOne);
            var newEntry = new TodoEntry()
            {
                DueDate = new DateTime(2017,08,01),
                Priority = TodoEntryPriorities.Low,
                Text = "New Entry",
                UserID = 1
            };
            todoEntryRepository.Setup(r => r.Add(newEntry)).Verifiable();

            // Act
            var success = todoEntryService.CreateTodoEntry(newEntry);

            // Assert
            Mock.Verify(new Mock[] { todoEntryRepository });
            Assert.AreEqual(true, success);

        }

        [TestMethod]
        public void Add_Valid_Entry_To_User_TodoList_Should_Not_Succeed_When_Throws_Exception()
        {
            // Arrange
            validationDictionary.Setup(r => r.IsValid).Returns(true);
            userRepository.Setup(r => r.GetById(1)).Returns(DataHelpers.UserOne);
            var newEntry = new TodoEntry()
            {
                DueDate = new DateTime(2017, 08, 01),
                Priority = TodoEntryPriorities.Low,
                Text = "New Entry",
                UserID = 1
            };
            todoEntryRepository.Setup(r => r.Add(newEntry)).Throws(new Exception());

            // Act
            var success = todoEntryService.CreateTodoEntry(newEntry);

            // Assert
            Assert.AreEqual(false, success);
        }


        [TestMethod]
        public void Add_Invalid_Entry_To_User_TodoList_Should_Not_Succeed()
        {
            // Arrange
            validationDictionary.Setup(r => r.AddError("Text", "Text is required.")).Verifiable();
            validationDictionary.Setup(r => r.AddError("UserID", "UserID is required.")).Verifiable();
            validationDictionary.Setup(r => r.IsValid).Returns(false);
            userRepository.Setup(r => r.GetById(1)).Returns(DataHelpers.UserOne);
            var newEntry = new TodoEntry()
            {
                Text = String.Empty,
                UserID = 0
            };

            // Act
            var success = todoEntryService.CreateTodoEntry(newEntry);

            // Assert
            Mock.Verify(new Mock[] { validationDictionary});
            Assert.AreEqual(false, success);
        }

        [TestMethod]
        public void Update_Valid_Entry_From_User_TodoList()
        {
            // Arrange
            validationDictionary.Setup(r => r.IsValid).Returns(true);
            userRepository.Setup(r => r.GetById(1)).Returns(DataHelpers.UserOne);
            var entryToUpdate = new TodoEntry()
            {
                DueDate = new DateTime(2017, 08, 01),
                Priority = TodoEntryPriorities.Low,
                Text = "Updated Entry",
                UserID = 1
            };
            todoEntryRepository.Setup(r => r.Update(entryToUpdate)).Verifiable();

            // Act
            var success = todoEntryService.UpdateTodoEntry(entryToUpdate);

            // Assert
            Mock.Verify(new Mock[] { todoEntryRepository });
            Assert.AreEqual(true, success);
        }

        [TestMethod]
        public void Update_Valid_Entry_From_User_TodoList_Should_Not_Succeed_When_Throws_Exception()
        {
            // Arrange
            validationDictionary.Setup(r => r.IsValid).Returns(true);
            userRepository.Setup(r => r.GetById(1)).Returns(DataHelpers.UserOne);
            var entryToUpdate = new TodoEntry()
            {
                DueDate = new DateTime(2017, 08, 01),
                Priority = TodoEntryPriorities.Low,
                Text = "Updated Entry",
                UserID = 1
            };
            todoEntryRepository.Setup(r => r.Update(entryToUpdate)).Throws(new Exception());

            // Act
            var success = todoEntryService.UpdateTodoEntry(entryToUpdate);

            // Assert
            Assert.AreEqual(false, success);
        }

        [TestMethod]
        public void Update_Invalid_Entry_From_User_TodoList_Should_Fail()
        {
            // Arrange
            validationDictionary.Setup(r => r.AddError("Text", "Text is required.")).Verifiable();
            validationDictionary.Setup(r => r.AddError("UserID", "UserID is required.")).Verifiable();
            validationDictionary.Setup(r => r.IsValid).Returns(false);
            userRepository.Setup(r => r.GetById(1)).Returns(DataHelpers.UserOne);
            var entryToUpdate = new TodoEntry()
            {
                Text = String.Empty,
                UserID = 0
            };

            // Act
            var success = todoEntryService.UpdateTodoEntry(entryToUpdate);

            // Assert
            Mock.Verify(new Mock[] { validationDictionary });
            Assert.AreEqual(false, success);
        }

        [TestMethod]
        public void Delete_Entry_From_User_TodoList()
        {
            // Arrange
            userRepository.Setup(r => r.GetById(1)).Returns(DataHelpers.UserOne);
            var entryToDelete = new TodoEntry()
            {
                DueDate = new DateTime(2017, 08, 01),
                Priority = TodoEntryPriorities.Low,
                Text = "New Entry",
                UserID = 1
            };
            todoEntryRepository.Setup(r => r.Delete(entryToDelete)).Verifiable();

            // Act
            var success = todoEntryService.DeleteTodoEntry(entryToDelete);

            // Assert
            Mock.Verify(new Mock[] { todoEntryRepository });
            Assert.AreEqual(true, success);
        }

        [TestMethod]
        public void Delete_Entry_From_User_TodoList_Should_Not_Succeed_When_Throws_Exception()
        {
            // Arrange
            userRepository.Setup(r => r.GetById(1)).Returns(DataHelpers.UserOne);
            var entryToDelete = new TodoEntry()
            {
                DueDate = new DateTime(2017, 08, 01),
                Priority = TodoEntryPriorities.Low,
                Text = "New Entry",
                UserID = 1
            };
            todoEntryRepository.Setup(r => r.Delete(entryToDelete)).Throws(new Exception());

            // Act
            var success = todoEntryService.DeleteTodoEntry(entryToDelete);

            // Assert
            Assert.AreEqual(false, success);
        }

        [TestMethod]
        public void Save_Entry()
        {
            // Arrange
            unitOfWork.Setup(r => r.Commit()).Verifiable();

            // Act
            var success = todoEntryService.SaveTodoEntry();

            // Assert
            Mock.Verify(new Mock[] { unitOfWork });
            Assert.AreEqual(true, success);
        }

        [TestMethod]
        public void Save_Entry_Should_Not_Succeed_When_Throws_Exception()
        {
            // Arrange
            unitOfWork.Setup(r => r.Commit()).Throws(new Exception());

            // Act
            var success = todoEntryService.SaveTodoEntry();

            // Assert
            Mock.Verify(new Mock[] { unitOfWork });
            Assert.AreEqual(false, success);
        }

        [TestMethod]
        public void MarkAsCompleted_Entry()
        {
            // Arrange
            validationDictionary.Setup(r => r.IsValid).Returns(true);
            var entryToMark = new TodoEntry()
            {
                DueDate = new DateTime(2017, 08, 01),
                Priority = TodoEntryPriorities.Low,
                Text = "Updated Entry",
                UserID = 1,
                Completed = false
            };
            todoEntryRepository.Setup(r => r.GetById(1)).Returns(entryToMark);
            todoEntryRepository.Setup(r => r.Update(It.Is<TodoEntry>(t => t.Completed))).Verifiable();

            // Act
            var success = todoEntryService.MarkAsCompleted(1);

            // Assert
            Mock.Verify(new Mock[] { todoEntryRepository });
            Assert.AreEqual(true, success);
        }

        [TestMethod]
        public void MarkAsCompleted_Entry_Should_Not_Succeed_When_Throws_Exception()
        {
            // Arrange
            var entryToMark = new TodoEntry()
            {
                DueDate = new DateTime(2017, 08, 01),
                Priority = TodoEntryPriorities.Low,
                Text = "Updated Entry",
                UserID = 1,
                Completed = false
            };
            todoEntryRepository.Setup(r => r.GetById(1)).Returns(entryToMark);
            todoEntryRepository.Setup(r => r.Update(It.Is<TodoEntry>(t => t.Completed))).Throws(new Exception());


            // Act
            var success = todoEntryService.MarkAsCompleted(1);

            // Assert
            Assert.AreEqual(false, success);
        }

        [TestMethod]
        public void MarkAsCompleted_Entry_Should_Not_Succeed_When_Could_Not_Found_An_Entry()
        {
            // Arrange
            var entryToMark = new TodoEntry()
            {
                DueDate = new DateTime(2017, 08, 01),
                Priority = TodoEntryPriorities.Low,
                Text = "Updated Entry",
                UserID = 1,
                Completed = false
            };
            todoEntryRepository.Setup(r => r.GetById(1)).Returns(entryToMark);
            todoEntryRepository.Setup(r => r.Update(It.Is<TodoEntry>(t => t.Completed))).Throws(new Exception());


            // Act
            var success = todoEntryService.MarkAsCompleted(2);

            // Assert
            Assert.AreEqual(false, success);
        }
    }
}
