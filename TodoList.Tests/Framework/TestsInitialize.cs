﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TodoList.Tests.Framework
{
    [TestClass]
    public class TestsInitialize
    {
        [AssemblyInitialize]
        public static void AssemblyInit(TestContext context)
        {
            Effort.Provider.EffortProviderConfiguration.RegisterProvider();
        }
    }
}
