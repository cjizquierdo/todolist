﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodoList.Data;
using TodoList.Model.Models;

namespace TodoList.Tests.Framework
{
    public static class DataHelpers
    {
        public static User UserOne
        {
            get
            {
                var user = new User
                {
                    UserID = 1,
                    Name = "User One",
                    Email = "email@somedomain.com",
                    Password = "password"
                };
                user.TodoList.Add(
                    new TodoEntry
                    {
                        Text = "Task 1",
                        Priority = TodoEntryPriorities.Medium,
                        DueDate = DateTime.Today.AddDays(10),
                        Completed = false
                    }
                );
                user.TodoList.Add(
                    new TodoEntry
                    {
                        Text = "Task 2",
                        Priority = TodoEntryPriorities.High,
                        DueDate = DateTime.Today.AddDays(2),
                        Completed = true
                    }
                );
                user.TodoList.Add(
                    new TodoEntry
                    {
                        Text = "Task 3",
                        Priority = TodoEntryPriorities.Low,
                        DueDate = DateTime.Today.AddDays(3),
                        Completed = false
                    }
                );
                return user;
            }
        }

        public static void InitializeData()
        {
            using (var db = new TodoListEntities())
            {
                User user = UserOne;
                db.Users.Add(user);

                user = new User
                {
                    Name = "User Two",
                    Email = "email@somedomain.com",
                    Password = "password"
                };
                user.TodoList.Add(
                    new TodoEntry
                    {
                        Text = "Task 1",
                        Priority = TodoEntryPriorities.Low,
                        DueDate = DateTime.Today.AddDays(1),
                        Completed = true
                    }
                );
                db.Users.Add(user);

                db.SaveChanges();
            }
        }

    }
}
