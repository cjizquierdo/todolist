﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TodoList.Model.Models
{
    public class TodoEntry
    {
        public TodoEntry()
        {
            DueDate = DateTime.Today;
            Completed = false;
        }

        public int TodoEntryID { get; set; }
        public string Text { get; set; }
        public DateTime DueDate { get; set; }
        public TodoEntryPriorities Priority { get; set; }
        public bool Completed { get; set; }
        public int UserID { get; set; }
        public User User { get; set; }
    }

    public enum TodoEntryPriorities
    {
        Low = 1,
        Medium = 2,
        High = 3
    }
}
