﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TodoList.Model.Models
{
    public class User
    {
        public User()
        {
            TodoList = new List<TodoEntry>();
        }

        public int UserID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public virtual List<TodoEntry> TodoList { get; set; }
    }
}
