﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TodoList.Model.Models
{
    public class SortProperty
    {
        public SortProperty(string propertyName, SortPropertyDirection direction)
        {
            PropertyName = propertyName;
            Direction = direction;
        }

        public string PropertyName {  get; set; }
        public SortPropertyDirection Direction { get; set; }
    }

    public enum SortPropertyDirection
    {
        Ascending,
        Descending
    }
}
