﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TodoList.Web.ViewModels
{
    public class TodoEntryViewModel
    {
        public int TodoEntryID { get; set; }
        [Required]
        [MaxLength(50)]
        public string Text { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime DueDate { get; set; }
        public string Priority { get; set; }
        public string Completed { get; set; }
        public int UserID { get; set; }
    }
}