﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TodoList.Web.ViewModels
{
    public class TodoListViewModel
    {
        public UserViewModel User { get; set; }
        public string SortedBy { get; set; }
        public TodoListSortertDirection SortDirection { get; set; }

        public static TodoListSortertDirection ToggleSortDirection(string sortDirection)
        {
            TodoListSortertDirection newSortDirection;
            Enum.TryParse<TodoListSortertDirection>(sortDirection, out newSortDirection);
            return newSortDirection == TodoListSortertDirection.Ascending ? TodoListSortertDirection.Descending : TodoListSortertDirection.Ascending;
        }
    }
    public enum TodoListSortertDirection
    {
        Ascending,
        Descending
    }

}