﻿using Autofac;
using Autofac.Builder;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using System.Linq;
using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;
using TodoList.Data.Infrastructure;
using TodoList.Data.Repositories;
using TodoList.Service;
using TodoList.Web.Controllers;
using TodoList.Web.Mappings;
using TodoList.Web.Models;

namespace TodoList.Web.App_Start
{
    public class Bootstrapper
    {
        public delegate ValidationDictionaryAdapter ValidationDictionaryAdapterFactoryDelegate(ModelStateDictionary modelState);
        public delegate ApiValidationDictionaryAdapter ApiValidationDictionaryAdapterFactoryDelegate(System.Web.Http.ModelBinding.ModelStateDictionary modelState);
        public delegate UserService UserServiceFactoryDelegate(IValidationDictionary validationDictionary);
        public delegate TodoEntryService TodoEntryServiceFactoryDelegate(IValidationDictionary validationDictionary);

        public static void Run()
        {
            // Set Dependency Injection
            SetAutofacContainer();

            //Configure AutoMapper
            AutoMapperConfiguration.Configure();
        }

        private static void SetAutofacContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<ValidationDictionaryAdapter>();
            builder.RegisterGeneratedFactory<ValidationDictionaryAdapterFactoryDelegate>();
            builder.RegisterType<ApiValidationDictionaryAdapter>();
            builder.RegisterGeneratedFactory<ApiValidationDictionaryAdapterFactoryDelegate>();
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerRequest();
            builder.RegisterType<DbFactory>().As<IDbFactory>().InstancePerRequest();

            // Api Controllers
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            // Controllers
            builder.RegisterControllers(Assembly.GetExecutingAssembly());

            // Repositories
            builder.RegisterAssemblyTypes(typeof(TodoEntryRepository).Assembly)
                .Where(t => t.Name.EndsWith("Repository"))
                .AsImplementedInterfaces().InstancePerRequest();

            // Services
            builder.RegisterType<UserService>();
            builder.RegisterGeneratedFactory<UserServiceFactoryDelegate>();
            builder.RegisterType<TodoEntryService>();
            builder.RegisterGeneratedFactory<TodoEntryServiceFactoryDelegate>();

            IContainer container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

            // Set the dependency resolver to be Autofac.
            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }
}