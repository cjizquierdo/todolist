﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TodoList.Model.Models;
using TodoList.Web.ViewModels;

namespace TodoList.Web.Mappings
{
    public class ModelToViewModelMappingProfile : Profile
    {
        public ModelToViewModelMappingProfile()
        {
            CreateMap<TodoEntry, TodoEntryViewModel>()
                .ForMember(destination => destination.Completed, option => option.MapFrom(source => source.Completed ? "Yes" : "No"));
            CreateMap<User, UserViewModel>()
                .ForMember(destination => destination.Password, option => option.MapFrom(source => String.Empty));
            ;
        }
        public override string ProfileName
        {
            get { return "ModelToViewModelMappingProfile"; }
        }
    }
}