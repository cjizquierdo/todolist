﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TodoList.Model.Models;
using TodoList.Web.ViewModels;

namespace TodoList.Web.Mappings
{
    public class ViewModelToModelMappingProfile : Profile
    {
        public ViewModelToModelMappingProfile()
        {
            CreateMap<TodoEntryViewModel, TodoEntry>()
                .ForMember(destination => destination.Completed, option => option.MapFrom(source => source.Completed == "Yes")); 
        }

        public override string ProfileName
        {
            get { return "ViewModelToModelMappingProfile"; }
        }
    }
}