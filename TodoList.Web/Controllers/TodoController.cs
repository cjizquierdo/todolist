﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TodoList.Model.Models;
using TodoList.Service;
using TodoList.Web.ViewModels;
using static TodoList.Web.App_Start.Bootstrapper;

namespace TodoList.Web.Controllers
{
    [Authorize]
    public class TodoController : Controller
    {
        private readonly IUserService userService;
        private readonly ITodoEntryService todoEntryService;

        public TodoController(UserServiceFactoryDelegate userServiceFactoryDelegate, TodoEntryServiceFactoryDelegate todoEntryServiceFactoryDelegate, ValidationDictionaryAdapterFactoryDelegate validationDictionaryAdapterFactoryDelegate)
        {
            this.userService = userServiceFactoryDelegate(validationDictionaryAdapterFactoryDelegate(this.ModelState));
            this.todoEntryService = todoEntryServiceFactoryDelegate(validationDictionaryAdapterFactoryDelegate(this.ModelState));
        }

        // GET: Todo
        public ActionResult Index(string sortOrder, string sortDirection)
        {
            User user = userService.GetUserByEmail(User.Identity.Name, sortOrder, sortDirection);

            if (user == null)
            {
                return View("Error");
            }
            UserViewModel userViewModel = Mapper.Map<User, UserViewModel>(user);
            var todoListViewModel = new TodoListViewModel
            {
                User = userViewModel,
                SortDirection = TodoListViewModel.ToggleSortDirection(sortDirection),
                SortedBy = "DueDate"
            };
            return View(todoListViewModel);
        }

        // GET: Todo/AddNew
        public ActionResult Add()
        {
            return View();
        }

        // POST: Todo/AddNew
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(int id, TodoEntryViewModel newEntry)
        {
            if (ModelState.IsValid)
            {
                var newTodoEntry = Mapper.Map<TodoEntryViewModel, TodoEntry>(newEntry);
                newTodoEntry.UserID = id;
                if (!todoEntryService.CreateTodoEntry(newTodoEntry))
                {
                    return View("Error");
                }
                if (!todoEntryService.SaveTodoEntry())
                {
                    return View("Error");
                }
                return RedirectToAction("Index");
            }

            return View(newEntry);
        }

        // GET: Todo/Edit/5
        public ActionResult Edit(int id)
        {
            var entryToEdit = todoEntryService.Get(id);
            var entry = Mapper.Map<TodoEntry, TodoEntryViewModel>(entryToEdit);
            return View(entry);
        }

        // POST: Todo/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TodoEntryViewModel editedEntry)
        {
            if (ModelState.IsValid)
            {
                var editedTodoEntry = Mapper.Map<TodoEntryViewModel, TodoEntry>(editedEntry);
                if (!todoEntryService.UpdateTodoEntry(editedTodoEntry))
                {
                    return View("Error");
                }
                if (!todoEntryService.SaveTodoEntry())
                {
                    return View("Error");
                }
                return RedirectToAction("Index");
            }
            return View(editedEntry);
        }

        // GET: Todo/Delete/1
        public ActionResult Delete(int id)
        {
            var entryToDelete = todoEntryService.Get(id);
            var entry = Mapper.Map<TodoEntry, TodoEntryViewModel>(entryToDelete);
            return View(entry);
        }

        // POST: Todo/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var entryToDelete = todoEntryService.Get(id);
            if (!todoEntryService.DeleteTodoEntry(entryToDelete))
            {
                return View("Error");
            }
            if (!todoEntryService.SaveTodoEntry())
            {
                return View("Error");
            }
            return RedirectToAction("Index");
        }

        // GET: Todo/MarkCompleted/1
        public ActionResult MarkCompleted(int id)
        {
            if (!todoEntryService.MarkAsCompleted(id))
            {
                return View("Error");
            }
            if (!todoEntryService.SaveTodoEntry())
            {
                return View("Error");
            }
            return RedirectToAction("Index");
        }

    }
}