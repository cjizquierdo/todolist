﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TodoList.Model.Models;
using TodoList.Service;
using TodoList.Web.ViewModels;
using static TodoList.Web.App_Start.Bootstrapper;

namespace TodoList.Web.Controllers
{
    [Authorize]
    public class ApiTodoController : ApiController
    {
        private readonly IUserService userService;
        private readonly ITodoEntryService todoEntryService;

        public ApiTodoController(UserServiceFactoryDelegate userServiceFactoryDelegate, TodoEntryServiceFactoryDelegate todoEntryServiceFactoryDelegate, ApiValidationDictionaryAdapterFactoryDelegate validationDictionaryAdapterFactoryDelegate)
        {
            this.userService = userServiceFactoryDelegate(validationDictionaryAdapterFactoryDelegate(base.ActionContext.ModelState));
            this.todoEntryService = todoEntryServiceFactoryDelegate(validationDictionaryAdapterFactoryDelegate(base.ActionContext.ModelState));
        }

        // GET api/todo
        public IHttpActionResult Get(string sortOrder, string sortDirection)
        {
            User user = userService.GetUserByEmail(User.Identity.Name, sortOrder, sortDirection);

            if (user == null)
            {
                return NotFound();
            }
            UserViewModel userViewModel = Mapper.Map<User, UserViewModel>(user);

            return Ok(userViewModel);

        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}