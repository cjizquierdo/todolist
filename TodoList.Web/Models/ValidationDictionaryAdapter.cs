﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TodoList.Service;

namespace TodoList.Web.Models
{
    public class ValidationDictionaryAdapter : IValidationDictionary
    {
        private ModelStateDictionary modelState;

        public ValidationDictionaryAdapter(ModelStateDictionary modelState)
        {
            this.modelState = modelState;
        }

        public void AddError(string key, string errorMessage)
        {
            modelState.AddModelError(key, errorMessage);
        }

        public bool IsValid
        {
            get { return modelState.IsValid; }
        }

    }
}