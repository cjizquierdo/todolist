﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.ModelBinding;
using TodoList.Service;

namespace TodoList.Web.Models
{
    public class ApiValidationDictionaryAdapter : IValidationDictionary
    {
        private ModelStateDictionary modelState;

        public ApiValidationDictionaryAdapter(ModelStateDictionary modelState)
        {
            this.modelState = modelState;
        }

        public void AddError(string key, string errorMessage)
        {
            modelState.AddModelError(key, errorMessage);
        }

        public bool IsValid
        {
            get { return modelState.IsValid; }
        }

    }
}