﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodoList.Model.Models;

namespace TodoList.Service
{
    public interface ITodoEntryService
    {
        TodoEntry Get(int todoEntryID);
        IEnumerable<TodoEntry> GetUserTodoList(int userID);
        bool CreateTodoEntry(TodoEntry todoEntry);
        bool UpdateTodoEntry(TodoEntry todoEntry);
        bool DeleteTodoEntry(TodoEntry todoEntry);
        bool SaveTodoEntry();
        bool MarkAsCompleted(int todoEntryID);
    }
}
