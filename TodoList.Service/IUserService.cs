﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodoList.Model.Models;

namespace TodoList.Service
{
    public interface IUserService
    {
        User Get(int userID);
        User GetUserByEmail(string email, string sortOrder, string sortDirection);
        bool IsValidLogin(string email, string password);
    }
}
