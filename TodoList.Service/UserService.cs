﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodoList.Data.Infrastructure;
using TodoList.Data.Repositories;
using TodoList.Model.Models;

namespace TodoList.Service
{
    public class UserService : IUserService
    {
        private readonly IValidationDictionary validationDictionary;
        private readonly IUserRepository usersRepository;
        private readonly ITodoEntryRepository todoEntryRepository;
        private readonly IUnitOfWork unitOfWork;

        public UserService(IValidationDictionary validationDictionary, IUserRepository usersRepository, ITodoEntryRepository todoEntryRepository, IUnitOfWork unitOfWork)
        {
            this.validationDictionary = validationDictionary;
            this.usersRepository = usersRepository;
            this.todoEntryRepository = todoEntryRepository;
            this.unitOfWork = unitOfWork;
        }

        public User Get(int id)
        {
            try
            {
                return usersRepository.GetById(id);
            }
            catch (Exception exc)
            {
                Trace.TraceError("Exception while geting User: {0}", exc);
                return null;
            }
        }

        private SortProperty BuildSortProperty(string sortOrder, string sortDirection)
        {
            SortPropertyDirection direction;
            Enum.TryParse<SortPropertyDirection>(sortDirection, out direction);
            var sortedBy = new SortProperty(String.IsNullOrEmpty(sortOrder) ? "DueDate" : sortOrder, direction);
            return sortedBy;
        }

        public User GetUserByEmail(string email, string sortOrder, string sortDirection)
        {
            try
            {
                var user = usersRepository.Get(p => p.Email == email);
                SortProperty sortedBy = BuildSortProperty(sortOrder, sortDirection);
                user.TodoList = todoEntryRepository.GetAll(new SortProperty[] { sortedBy }).ToList();
                return user;
            }
            catch (Exception exc)
            {
                Trace.TraceError("Exception while geting User by email : {0}", exc);
                return null;
            }
        }

        public bool IsValidLogin(string email, string password)
        {
            try
            {
                return usersRepository.IsValidLogin(email, password);
            }
            catch (Exception exc)
            {
                Trace.TraceError("Exception while login : {0}", exc);
                return false;
            }
        }
    }
}
