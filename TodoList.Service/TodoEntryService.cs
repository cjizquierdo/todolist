﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodoList.Data.Infrastructure;
using TodoList.Data.Repositories;
using TodoList.Model.Models;

namespace TodoList.Service
{
    public class TodoEntryService : ITodoEntryService
    {
        private readonly IValidationDictionary validationDictionary;
        private readonly IUserRepository usersRepository;
        private readonly ITodoEntryRepository todoEntriesRepository;
        private readonly IUnitOfWork unitOfWork;

        public TodoEntryService(IValidationDictionary validationDictionary, IUserRepository usersRepository, ITodoEntryRepository todoEntriesRepository, IUnitOfWork unitOfWork)
        {
            this.validationDictionary = validationDictionary;
            this.usersRepository = usersRepository;
            this.todoEntriesRepository = todoEntriesRepository;
            this.unitOfWork = unitOfWork;
        }
        public TodoEntry Get(int id)
        {
            try
            {
                return todoEntriesRepository.GetById(id);
            }
            catch (Exception exc)
            {
                Trace.TraceError("Exception while geting Todo Entry: {0}", exc);
                return null;
            }
        }
        public bool CreateTodoEntry(TodoEntry todoEntry)
        {
            if (!ValidateTodoEntry(todoEntry))
            {
                return false;
            }

            try
            {
                todoEntriesRepository.Add(todoEntry);
            }
            catch(Exception exc)
            {
                Trace.TraceError("Exception while creating TodoEntry: {0}", exc);
                return false;
            }

            return true;
        }

        protected bool ValidateTodoEntry(TodoEntry todoEntry)
        {
            if (String.IsNullOrEmpty(todoEntry.Text))
            {
                validationDictionary.AddError("Text", "Text is required.");
            }

            if ((todoEntry.Text != null) && (todoEntry.Text.Length > 50))
            {
                validationDictionary.AddError("Text", "Text max length must be 50 characters.");
            }

            if (todoEntry.UserID <= 0)
            {
                validationDictionary.AddError("UserID", "UserID is required.");
            }

            return validationDictionary.IsValid;
        }

        public bool UpdateTodoEntry(TodoEntry todoEntry)
        {
            if (!ValidateTodoEntry(todoEntry))
            {
                return false;
            }

            try
            {
                todoEntriesRepository.Update(todoEntry);
            }
            catch (Exception exc)
            {
                Trace.TraceError("Exception while updating TodoEntry: {0}", exc);
                return false;
            }

            return true;

        }
        public bool DeleteTodoEntry(TodoEntry todoEntry)
        {
            try
            {
                todoEntriesRepository.Delete(todoEntry);
            }
            catch (Exception exc)
            {
                Trace.TraceError("Exception while deleting TodoEntry: {0}", exc);
                return false;
            }

            return true;
        }

        public IEnumerable<TodoEntry> GetUserTodoList(int id)
        {
            try
            {
                return usersRepository.GetById(id).TodoList;
            }
            catch (Exception exc)
            {
                Trace.TraceError("Exception while getting TodoEntry: {0}", exc);
                return null;
            }
        }

        public bool SaveTodoEntry()
        {
            try
            {
                unitOfWork.Commit();
            }
            catch (Exception exc)
            {
                Trace.TraceError("Exception while saving TodoEntry: {0}", exc);
                return false;
            }

            return true;
        }

        public bool MarkAsCompleted(int todoEntryID)
        {
            try
            {
                var todoEntry = todoEntriesRepository.GetById(todoEntryID);

                if (todoEntry == null)
                {
                    Trace.TraceError("TodoEntryID: {0}, could not be found while marking as completed.", todoEntryID);
                    return false;
                }

                todoEntry.Completed = true;
                todoEntriesRepository.Update(todoEntry);
            }
            catch (Exception exc)
            {
                Trace.TraceError("Exception while marking as completed TodoEntry: {0}", exc);
                return false;
            }
            return true;
        }
    }
}
