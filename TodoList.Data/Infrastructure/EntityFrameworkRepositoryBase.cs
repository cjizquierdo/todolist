﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TodoList.Data.Infrastructure.Util;
using TodoList.Model.Models;

namespace TodoList.Data.Infrastructure
{
    public abstract class EntityFrameworkRepositoryBase<T> where T : class
    {
        private TodoListEntities dataContext;
        private readonly IDbSet<T> dbSet;
        private readonly IDbFactory dbFactory;

        protected TodoListEntities DbContext
        {
            get { return dataContext ?? (dataContext = dbFactory.Init() as TodoListEntities); }
        }
        protected EntityFrameworkRepositoryBase(IDbFactory dbFactory)
        {
            this.dbFactory = dbFactory;
            dbSet = DbContext.Set<T>();
        }

        public virtual void Add(T entity)
        {
            dbSet.Add(entity);
        }

        public virtual void Update(T entity)
        {
            dbSet.Attach(entity);
            dataContext.Entry(entity).State = EntityState.Modified;
        }

        public virtual void Delete(T entity)
        {
            dbSet.Remove(entity);
        }

        public virtual T GetById(int id)
        {
            return dbSet.Find(id);
        }
        public T Get(Expression<Func<T, bool>> where)
        {
            return dbSet.Where(where).FirstOrDefault<T>();
        }

        public virtual IEnumerable<T> GetAll(SortProperty[] sortByProperties = null)
        {
            var query = dbSet.AsQueryable();

            if (sortByProperties != null)
            {
                var entityType = query.GetType().GetGenericArguments()[0];
                foreach (var sortProperty in sortByProperties)
                {
                    var propertyInfo = entityType.GetProperty(sortProperty.PropertyName);

                    if (propertyInfo == null)
                    {
                        throw new ArgumentException(String.Format("'{0}' is not a valid sort property", sortProperty.PropertyName), "sortByProperties");
                    }

                    switch (sortProperty.Direction)
                    {
                        case SortPropertyDirection.Ascending:
                            query = query.AppendOrderByProperty(sortProperty.PropertyName).AsQueryable();
                            break;
                        case SortPropertyDirection.Descending:
                            query = query.AppendOrderByPropertyDescending(sortProperty.PropertyName).AsQueryable();
                            break;
                    }
                }
            }

            return query.AsEnumerable();
        }
    }
}
