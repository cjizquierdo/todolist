﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TodoList.Data.Infrastructure
{
    public class DbFactory : IDisposable, IDbFactory
    {
        TodoListEntities dbContext;

        public DbContext Init()
        {
            return dbContext ?? (dbContext = new TodoListEntities());
        }

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue && disposing)
            {
                if (dbContext != null)
                    dbContext.Dispose();
            }

            disposedValue = true;
        }

         ~DbFactory()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
