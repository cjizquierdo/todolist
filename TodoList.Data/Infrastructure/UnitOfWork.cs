﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TodoList.Data.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IDbFactory dbFactory;
        private TodoListEntities dbContext;

        public UnitOfWork(IDbFactory dbFactory)
        {
            this.dbFactory = dbFactory;
        }

        public TodoListEntities DbContext
        {
            get { return dbContext ?? (dbContext = dbFactory.Init() as TodoListEntities); }
        }

        public void Commit()
        {
            DbContext.Commit();
        }
    }
}
