﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodoList.Model.Models;

namespace TodoList.Data
{
    public class TodoListSeedData : DropCreateDatabaseIfModelChanges<TodoListEntities>
    {
        protected override void Seed(TodoListEntities context)
        {
            GetUsers().ForEach(g => context.Users.Add(g));

            context.Commit();
        }

        private static List<User> GetUsers()
        {
            var users = new List<User>();
            var user = new User
            {
                UserID = 1,
                Name = "User One",
                Email = "email@somedomain.com",
                Password = "password"
            };
            user.TodoList.Add(
                new TodoEntry
                {
                    Text = "Task 1",
                    Priority = TodoEntryPriorities.Medium,
                    DueDate = DateTime.Today.AddDays(10),
                    Completed = false
                }
            );
            user.TodoList.Add(
                new TodoEntry
                {
                    Text = "Task 2",
                    Priority = TodoEntryPriorities.High,
                    DueDate = DateTime.Today.AddDays(2),
                    Completed = true
                }
            );
            user.TodoList.Add(
                new TodoEntry
                {
                    Text = "Task 3",
                    Priority = TodoEntryPriorities.Low,
                    DueDate = DateTime.Today.AddDays(3),
                    Completed = false
                }
            );

            users.Add(user);

            return users;
        }
    }
}
