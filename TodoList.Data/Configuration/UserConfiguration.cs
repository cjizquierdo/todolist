﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodoList.Model.Models;

namespace TodoList.Data.Configuration
{
    public class UserConfiguration:  EntityTypeConfiguration<User>
    {
        public UserConfiguration()
        {
            ToTable("Users");
            Property(g => g.Name).IsRequired().HasMaxLength(50);
            Property(g => g.Email).IsRequired().HasMaxLength(50);
            Property(g => g.Password).IsRequired().HasMaxLength(50);
        }
    }
}
