﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodoList.Model.Models;

namespace TodoList.Data.Configuration
{
    public class TodoEntryConfiguration:  EntityTypeConfiguration<TodoEntry>
    {
        public TodoEntryConfiguration()
        {
            ToTable("TodoEntries");
            Property(g => g.Text).IsRequired().HasMaxLength(50);
            Property(g => g.DueDate).IsRequired();
            Property(g => g.Priority).IsRequired();
        }
    }
}
