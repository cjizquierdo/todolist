﻿using TodoList.Data.Infrastructure;
using TodoList.Data.Infrastructure.Util;
using TodoList.Model.Models;
using System.Linq;
using System.Data.Entity;
using System.Collections.Generic;
using System;
using System.Linq.Expressions;

namespace TodoList.Data.Repositories
{
    public class TodoEntryRepository : EntityFrameworkRepositoryBase<TodoEntry>, ITodoEntryRepository
    {
        public TodoEntryRepository(IDbFactory dbFactory)
            : base(dbFactory) { }

        public override TodoEntry GetById(int id)
        {
            return this.DbContext.TodoEntries.Include(t => t.User).Where(x => x.TodoEntryID == id).SingleOrDefault();
        }
    }
}
