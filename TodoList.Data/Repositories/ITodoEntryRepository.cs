﻿using System.Collections.Generic;
using TodoList.Data.Infrastructure;
using TodoList.Model.Models;

namespace TodoList.Data.Repositories
{
    public interface ITodoEntryRepository : IRepository<TodoEntry>
    {
    }
}
