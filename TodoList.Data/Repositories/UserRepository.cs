﻿using System;
using TodoList.Data.Infrastructure;
using TodoList.Model.Models;
using System.Linq;

namespace TodoList.Data.Repositories
{
    public class UserRepository : EntityFrameworkRepositoryBase<User>, IUserRepository
    {
        public UserRepository(IDbFactory dbFactory)
            : base(dbFactory) { }

        public bool IsValidLogin(string email, string password)
        {
            var user = this.DbContext.Users.SingleOrDefault(u => u.Email.TrimEnd() == email && u.Password == password);
            return user != null;
        }
    }
}
