﻿using System.Linq.Expressions;
using TodoList.Data.Infrastructure;
using TodoList.Model.Models;

namespace TodoList.Data.Repositories
{
    public interface IUserRepository : IRepository<User>
    {
        bool IsValidLogin(string email, string password);
    }
}
