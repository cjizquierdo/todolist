﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodoList.Data.Configuration;
using TodoList.Model.Models;

namespace TodoList.Data
{
    public class TodoListEntities : DbContext
    {
        public TodoListEntities() : base("TodoListEntities") { }

        public DbSet<TodoEntry> TodoEntries { get; set; }
        public DbSet<User> Users { get; set; }

        public virtual void Commit()
        {
            base.SaveChanges();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new TodoEntryConfiguration());
            modelBuilder.Configurations.Add(new UserConfiguration());
        }
    }
}
